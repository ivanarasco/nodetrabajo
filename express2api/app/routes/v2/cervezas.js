const express = require('express')

// para establecer las distintas rutas, necesitamos instanciar el express router
var router = express.Router()
const cervezaController = require('../../controllers/v2/cervezaController.js')


router.get('/', (req, res) =>{
  console.log('vamos a pedir lista  de cervezas')
  cervezaController.index(req, res)
})


// router.get('/search', (req, res) =>{
//   console.log('vamos a BUSCAR lista  de cervezas')
//   cervezaController.search(req, res)
// })

router.get('/:id', (req, res) =>{
  console.log('vamos a MOSTRAR una cervezas')
  cervezaController.show(req, res)
})

router.post('/', (req, res) =>{
  console.log('vamos a CREAR una cervezas')
  cervezaController.create(req, res)
})

router.put('/:id', (req, res) =>{
  console.log('vamos a ACTUALIZAR una cervezas')
  cervezaController.update(req, res)
})

router.delete('/:id', (req, res) => {
  console.log('metodo DELETE  de cervezas')
  cervezaController.remove(req, res)
})

module.exports = router

