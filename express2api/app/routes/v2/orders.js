const express = require('express')

// para establecer las distintas rutas, necesitamos instanciar el express router
var router = express.Router()
const orderController = require('../../controllers/v2/orderController.js')
//const auth=require('../../middlewares/auth')

//router.use(auth.auth)

router.get('/', (req, res) =>{
  console.log('vamos a pedir lista  de ORDER')
  orderController.index(req, res)
})


// router.get('/search', (req, res) =>{
//   console.log('vamos a BUSCAR lista  de ORDER')
//   productController.search(req, res)
// })

router.get('/:id', (req, res) =>{
  console.log('vamos a MOSTRAR un ORDER')
  orderController.show(req, res)
})

router.post('/', (req, res) =>{
  console.log('vamos a CREAR un ORDER')
  orderController.create(req, res)
})

router.put('/:id', (req, res) =>{
  console.log('vamos a ACTUALIZAR un ORDER')
  orderController.update(req, res)
})

router.delete('/:id', (req, res) => {
  console.log('metodo DELETE  de ORDER')
  orderController.remove(req, res)
})

module.exports = router

