const express = require('express')
const routerCervezas =require('./routes/v2/cervezas.js')
const routerProducts =require('./routes/v2/products.js')
const routerUsers =require('./routes/v2/users.js')
const routerOrders =require('./routes/v2/orders.js')


var router = express.Router()

//establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API con MONGODB!' })  
})

// const Cerveza = require('./models/v2/Cerveza')

// router.get('/ambar', (req, res) => {
//   const miCerveza = new Cerveza ({name: 'Ambar'})
//   miCerveza.save((err, miCerveza) =>{
//     if(err) return console.error(err)
//     console.log(`Guardada en BBDD ${miCerveza.nombre}`)
//   })
// })

router.use('/products', routerProducts);
router.use('/cervezas', routerCervezas);
router.use('/users', routerUsers);
router.use('/orders', routerOrders);


module.exports=router;