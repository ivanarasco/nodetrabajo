const mongoose = require('mongoose')
const Schema = mongoose.Schema
const User= require('./User.js')

const orderSchema = new Schema({
  user_id:{
  type:Schema.Types.ObjectId,
  required:true,
  ref:'User'
  },
  created: {
    type:Date,
    default:Date.now
  }
})

const Order = mongoose.model('Order', orderSchema)

module.exports = Order

