const express = require('express')
const app = express()
const fs = require('fs')
const path = require('path')//para recursos estaticos
const morgan = require('morgan')

const hbs = require('hbs')
hbs.registerPartials(path.join(__dirname, 'views', 'partials'))
app.set('view engine', 'hbs'); // clave valor

//un par de middleware
//morgan desarrollado por terceros
app.use(morgan('tiny'))

app.use(function (req, res, next) {
    var now = new Date().toString()
    var log = `${now}: ${req.method} ${req.url}`
    console.log(log)
    fs.appendFile('server.log', `${log}\n`, (err) => {
      if (err) console.log(`No se ha podido usar el fichero de log:  ${err}`)
    })
    next()
  })


//middleware de contenido estatico
const publicRoute = path.join(__dirname, 'public')
app.use(express.static(publicRoute))

const staticRoute = path.join(__dirname, 'estatico')
app.use('/estatico', express.static(staticRoute))


// app.use((req, res, next)=>{
//     //operaciones del middleware
//     // res.send('Hasta aqui has llegado')
//     next() //para ir al siguiente middleware o a la ruta
//     // también podríamos hacer un send() y cortar
//     // la cola de middlewares, por ej en un control de permisos
//   })



app.get('/', (req, res) => {
    res.send('Hola Mundoo')
})

// const contacto =require ('./contacto.json')
const contacto =require ('./contacto.js')

app.get('/contactar', (req, res) =>{
    res.render('contactar.hbs', {
        pageTitle: 'Contactar',
        currentYear: new Date().getFullYear()
      })
    // res.write('Contacto')
    // res.write('Aqui tienes la URL')
    // res.send()
})


app.get('/contactar/pepe', (req, res) =>{
    res.json(contacto)
})

app.listen(3000, (err) =>{
    console.log('Servidor en el puerto 3000');
})//funcion callback