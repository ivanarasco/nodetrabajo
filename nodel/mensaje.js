
// module.exports='Hola Mundo';
// module.exports.saludo='Hola Mundo';
// module.exports.adios='Hasta la vista';

module.exports ={
    saludo:'Hola mundo',
    adios:'Hasta la vista',
    log: function (msg){
        console.log(msg);
    }
}


/*
module.exports.log = function (msg) {
    console.log(msg);
};*/


module.exports = function(saludo, adios){
    this.saludo=saludo;
    this.adios=adios;
    this.log = function(text){
        console.log(text);
    }
}
