const http = require('http');

//crear servidor:
http.createServer(function (req, res) {
  res.write('Hola mundo!'); //escribir respuesta al cliente
  res.end(); //cerrar respuesta
}).listen(8080, () => console.log("Servidor en localhost:8080"));
